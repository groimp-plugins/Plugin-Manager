package de.grogra.pm;

import java.io.IOException;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;

abstract public class JSONSource implements Cloneable {
    public abstract JSONArray getRepo() throws IOException;

    public abstract void setTimeout(int timeout);

    public abstract DownloadResult getZip(String id, String location, GenericCallback<String> statusChanged) throws IOException;

    public class DownloadResult {
        private final String tmpFile;
        private final String filename;

        public DownloadResult(String tmpFile, String filename) {
            this.tmpFile = tmpFile;
            this.filename = filename;
        }

        public String getTmpFile() {
            return tmpFile;
        }

        public String getFilename() {
            return filename;
        }
        
        public String getName() {
            return FilenameUtils.getBaseName(filename);
        }
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }
}
