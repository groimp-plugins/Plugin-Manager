package de.grogra.pm;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import de.grogra.pm.exception.PluginException;

public class DependencyResolver {
    private static final Pattern libNameParser = Pattern.compile("([^=<>]+)([=<>]+[0-9.a-zA-Z]+)?");
    protected final Set<PluginEntry> deletions = new HashSet<>();
    protected final Set<PluginEntry> additions = new HashSet<>();
    protected final Map<String, String> libAdditions = new HashMap<>();
    protected final Set<String> libDeletions = new HashSet<>();
    protected final Map<PluginEntry, Boolean> allPlugins;
    protected final Map<PluginEntry, PluginEntry> pluginExclusions = new HashMap<>();

    public DependencyResolver(Map<PluginEntry, Boolean> allPlugins) {
        this.allPlugins = allPlugins;
        resolve();
    }

    public void resolve() {
        clear();
        resolveFlags();
        resolveUpgrades();
        resolveDeleteByDependency();
        resolveInstallByDependency();
        resolveExclusions();
        resolveDeleteLibs();
        resolveInstallLibs();
        resolveUpgradesLibs();
        detectConflicts();
    }

    public void clear() {
        deletions.clear();
        additions.clear();
        libAdditions.clear();
        libDeletions.clear();
    }


    // TODO: return iterators to make values read-only
    public Set<PluginEntry> getDeletions() {
        return deletions;
    }

    public Set<PluginEntry> getAdditions() {
        return additions;
    }

    public Map<String, String> getLibAdditions() {
        return libAdditions;
    }

    public Set<String> getLibDeletions() {
        return libDeletions;
    }
    
    public Map<PluginEntry, PluginEntry> getPluginExclusions() {
        return pluginExclusions;
    }

    private PluginEntry getPluginByID(String id) {
        for (PluginEntry plugin : allPlugins.keySet()) {
            if (plugin.getID().equals(id)) {
                return plugin;
            }
        }
        throw new PluginException("Plugin not found by ID: " + id);
    }

    private Set<PluginEntry> getDependants(PluginEntry plugin) {
        Set<PluginEntry> res = new HashSet<>();
        for (PluginEntry pAll : allPlugins.keySet()) {
            for (String depID : pAll.getDepends()) {
                if (depID.equals(plugin.getID())) {
                    res.add(pAll);
                }
            }
        }
        return res;
    }
    
    /*
     * return true if at least one of the installed plugin exclude the plugin
     */
    private boolean isExcluded(PluginEntry plugin) {
    	Set<PluginEntry> installedPlugins = PluginManager.getInstalledPlugins(allPlugins);
    	for (PluginEntry p : installedPlugins) {
            if (p.getExcluded().contains(plugin.getID())) {
            	return true;
            }
    	}
    	return false;
    }


    private void resolveFlags() {
        for (Map.Entry<PluginEntry, Boolean> entry : allPlugins.entrySet()) {
            if (entry.getKey().isInstalled()) {
                if (!entry.getValue()) {
                    deletions.add(entry.getKey());
                }
            } else if (entry.getValue()) {
                additions.add(entry.getKey());
            }
        }
    }

    private void resolveUpgrades() {
        // detect upgrades
        for (Map.Entry<PluginEntry, Boolean> entry : allPlugins.entrySet()) {
        	PluginEntry plugin = entry.getKey();
            if (entry.getValue() && plugin.isInstalled() && !plugin.getInstalledVersion().equals(plugin.getCandidateVersion())) {
//                Main.getLogger().warning("Upgrade: " + plugin);
                deletions.add(plugin);
                additions.add(plugin);
            }
        }
    }

    private void resolveDeleteByDependency() {
        // delete by depend
        boolean hasModifications = true;
        while (hasModifications) {
//        	Main.getLogger().warning("Check uninstall dependencies");
            hasModifications = false;
            for (PluginEntry plugin : deletions) {
                if (!additions.contains(plugin)) {
                    for (PluginEntry dep : getDependants(plugin)) {
                        if (!deletions.contains(dep) && dep.isInstalled()) {
//                        	Main.getLogger().warning("Add to deletions: " + dep);
                            deletions.add(dep);
                            hasModifications = true;
                        }
                        if (additions.contains(dep)) {
//                        	Main.getLogger().warning("Remove from additions: " + dep);
                            additions.remove(dep);
                            hasModifications = true;
                        }
                    }
                }

                if (hasModifications) {
                    break; // prevent ConcurrentModificationException
                }
            }
        }
    }
    
    
    private void resolveExclusions() {
        // resolve exclusions
        boolean hasModifications = true;
        while (hasModifications) {
            hasModifications = false;
            for (PluginEntry plugin : additions) {
            	for (String pluginID : plugin.getExcluded()) {
            		PluginEntry excl = getPluginByID(pluginID);
            		if ( (excl.isInstalled() && !deletions.contains(excl)) 
            				|| (additions.contains(excl)) ) {
            			additions.remove(plugin);
            			pluginExclusions.put(plugin, excl);
            			hasModifications = true;
            			for (PluginEntry dep : getDependants(plugin)) {
            				if (additions.contains(dep)) {
                                additions.remove(dep);
                                pluginExclusions.put(dep, excl);
                            }
            			}
            		}
            	}
            	if (isExcluded(plugin) ) {
            		additions.remove(plugin);
            		pluginExclusions.put(plugin, null);
            		hasModifications = true;
            	}
                if (hasModifications) {
                    break; // prevent ConcurrentModificationException
                }
            }
        }
    }
    
    private void resolveInstallByDependency() {
        // resolve dependencies
        boolean hasModifications = true;
        while (hasModifications) {
//        	Main.getLogger().warning("Check install dependencies: " + additions);
            hasModifications = false;
            for (PluginEntry plugin : additions) {
                for (String pluginID : plugin.getDepends()) {
                	PluginEntry depend = getPluginByID(pluginID);

                    if (!depend.isInstalled() || deletions.contains(depend)) {
                        if (!additions.contains(depend)) {
//                        	Main.getLogger().warning("Add to install: " + depend);
                            additions.add(depend);
                            hasModifications = true;
                        }
                    }
                }

                if (hasModifications) {
                    break; // prevent ConcurrentModificationException
                }
            }
        }
    }

    private void resolveInstallLibs() {
//        for (PluginEntry plugin : additions) {
//            Map<String, String> libs = plugin.getLibs(plugin.getCandidateVersion());
//            for (String lib : libs.keySet()) {
//                resolveLibForPlugin(plugin, lib, libs.get(lib), true);
//            }
//        }
//        resolveLibsVersionsConflicts();
    }

    private void resolveLibForPlugin(PluginEntry plugin, String lib, String link, boolean useFullName) {
//        String installedPath = PluginEntry.getLibInstallPath(getLibName(lib));
//        if (installedPath == null) {
//            libAdditions.put(useFullName ? lib : getLibName(lib), link);
//        } else {
//            resolveUpdateLib(plugin, getLibrary(lib, ""), lib);
//        }
    }

    private void resolveUpdateLib(PluginEntry plugin, Library installedLib, String candidateLibName) {
//        final Map<String, String> candidateLibs = plugin.getLibs(plugin.getCandidateVersion());
//
//        // get candidate lib
//        Library candidateLib = getLibrary(candidateLibName, candidateLibs.get(candidateLibName));
//
//        // get installed lib version
//        String installedPath = PluginEntry.getLibInstallPath(installedLib.getName());
//        if (installedPath == null) {
//            libAdditions.put(candidateLib.getName(), candidateLib.getLink());
//            return;
//        }
//        String installedVersion = plugin.getInstalledVersion();
//        installedLib.setVersion(installedVersion);
//
//
//        // compare installed and candidate libs
//        if (candidateLib.getVersion() != null && Library.versionComparator.compare(installedLib, candidateLib) < 0) {
//            libDeletions.add(installedLib.getName());
//            libAdditions.put(candidateLib.getName(), candidateLib.getLink());
//        }
    }

    private void resolveDeleteLibs() {
//        for (PluginEntry plugin : deletions) {
//            if (additions.contains(plugin)) { // skip upgrades
//                continue;
//            }
//
//            Map<String, String> libs = plugin.getLibs(plugin.getInstalledVersion());
//            for (String lib : libs.keySet()) {
//                String name = getLibName(lib);
//                if (PluginEntry.getLibInstallPath(name) != null) {
//                    libDeletions.add(name);
//                } else {
//                    Main.getLogger().warning("Did not find library to uninstall it: " + lib);
//                }
//            }
//        }
//
//        for (PluginEntry plugin : allPlugins.keySet()) {
//            if (additions.contains(plugin) || (plugin.isInstalled() && !deletions.contains(plugin))) {
//                String ver = additions.contains(plugin) ? plugin.getCandidateVersion() : plugin.getInstalledVersion();
//                //log.debug("Affects " + plugin + " v" + ver);
//                Map<String, String> libs = plugin.getLibs(ver);
//                for (String lib : libs.keySet()) {
//                    String name = getLibName(lib);
//                    if (libDeletions.contains(name)) {
//                    	Main.getLogger().warning("Won't delete lib " + lib + " since it is used by " + plugin);
//                        libDeletions.remove(name);
//                    }
//                }
//            }
//        }
    }

    private void resolveUpgradesLibs() {
//        for (PluginEntry plugin : deletions) {
//            if (additions.contains(plugin)) { // if upgrade plugin
//                final Map<String, String> installedLibs = plugin.getLibs(plugin.getInstalledVersion());
//                final Map<String, String> candidateLibs = plugin.getLibs(plugin.getCandidateVersion());
//
//                for (String candidateLibName : candidateLibs.keySet()) {
//                    String installedLibName = getMatchLibName(candidateLibName, installedLibs);
//                    if (installedLibName != null) {
//                        resolveUpdateLib(plugin, getLibrary(installedLibName, installedLibs.get(installedLibName)), candidateLibName);
//                    }
//                }
//
//            }
//        }
    }

    private String getMatchLibName(String candidateLibName, Map<String, String> installedLibs) {
        final String candidateName = getLibName(candidateLibName);

        for (String installedLibName : installedLibs.keySet()) {
            if (installedLibName.startsWith(candidateName) && getLibName(installedLibName).equals(candidateName)) {
                return installedLibName;
            }
        }

        return null;
    }


    public static String getLibName(String fullLibName) {
        Matcher m = libNameParser.matcher(fullLibName);
        if (!m.find()) {
            throw new IllegalArgumentException("Cannot parse str: " + fullLibName);
        }
        return m.group(1);
    }

    private Library getLibrary(String fullLibName, String link) {
        Matcher m = libNameParser.matcher(fullLibName);
        if (!m.find()) {
            throw new IllegalArgumentException("Cannot parse str: " + fullLibName);
        }

        final String name = m.group(1);
        if (m.groupCount() == 2 && m.group(2) != null && !m.group(2).isEmpty()) {
            String condition = m.group(2).substring(0, 2);
            verifyConditionFormat(condition);
            String version = m.group(2).substring(2);

            return new Library(name, version, link);
        }
        return new Library(name, link);
    }

    private void resolveLibsVersionsConflicts() {
//        Map<String, List<Library>> libsToResolve = new HashMap<>();
//
//        for (String key : libAdditions.keySet()) {
//            Library library = getLibrary(key, libAdditions.get(key));
//            if (library.getVersion() != null) {
//                if (libsToResolve.containsKey(library.getName())) {
//                    libsToResolve.get(library.getName()).add(library);
//                } else {
//                    List<Library> libs = new ArrayList<>();
//                    libs.add(library);
//                    libsToResolve.put(library.getName(), libs);
//                }
//            }
//        }
//
//        for (String key : libsToResolve.keySet()) {
//            List<Library> libs = libsToResolve.get(key);
//            Collections.sort(libs, Library.versionComparator);
//
//            for (Library lib : libs) {
//                libAdditions.remove(lib.getFullName());
//            }
//
//            final Library libToInstall = libs.get(libs.size() - 1);
//            // override lib
//            libAdditions.put(libToInstall.getName(), libToInstall.getLink());
//        }
    }


    // TODO: manage '==' and '<=' condition
    protected void verifyConditionFormat(String condition) {
        if (!condition.equals(">=")) {
            throw new IllegalArgumentException("Expected conditions are ['>='], but was: " + condition);
        }
    }

    public void detectConflicts() {
        Set<PluginEntry> installedPlugins = PluginManager.getInstalledPlugins(allPlugins);

        for (PluginEntry plugin : installedPlugins) {
            Map<String, String> requiredLibs = plugin.getLibs(plugin.getInstalledVersion());
            for (String lib : requiredLibs.keySet()) {
                resolveLibForPlugin(plugin, lib, requiredLibs.get(lib), false);
            }
        }
    }
}
