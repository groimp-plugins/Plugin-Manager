package de.grogra.pm;


import org.json.JSONArray;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.net.URLDecoder;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import de.grogra.pf.boot.Main;
import de.grogra.pf.registry.Exclude;
import de.grogra.pf.registry.Item;
import de.grogra.pf.registry.ItemCriterion;
import de.grogra.pf.registry.Library;
import de.grogra.pf.registry.PluginDescriptor;
import de.grogra.pf.registry.PluginPrerequisite;
import de.grogra.pf.registry.Registry;
import de.grogra.pm.exception.DownloadException;

public class PluginEntry {
    private static final Pattern dependsParser = Pattern.compile("([^=<>]+)([=<>]+[0-9.]+)?");
    public static final String VER_STOCK = "0.0.0-STOCK";
    protected JSONObject versions = new JSONObject();
    protected String id;
    protected String name;
    protected String description;
    protected String helpUrl;
    protected String vendor;
    protected static String[] dependencies;
    protected static String[] libs;
    protected static String[] excluded;
    
    protected String mainClass;
    protected String installedPath;
    protected String installedVersion;
    protected String tempName;
    protected String destName;
    protected String candidateVersion;
//    protected String installerClass = null;
//    protected List<String> componentClasses;
    protected boolean canUninstall = true;
    private String searchIndexString;
    

    public PluginEntry(String aId) {
        id = aId;
    }

    public static PluginEntry fromJSON(JSONObject elm) {
    	PluginEntry inst = new PluginEntry(elm.getString("id"));
        if (!(elm.isNull("mainClass"))) {
            inst.mainClass = elm.getString("mainClass");
        }

        if (elm.get("versions") instanceof JSONObject) {
            inst.versions = elm.getJSONObject("versions");
        }
        inst.name = elm.getString("name");
        inst.description = elm.getString("description");
        inst.helpUrl = elm.getString("helpUrl");
        inst.vendor = elm.getString("vendor");
        if (elm.has("canUninstall")) {
            inst.canUninstall = elm.getBoolean("canUninstall");
        }
        return inst;
    }
    
    
    public static PluginEntry fromPluginDescriptor(PluginDescriptor pd) {
    	PluginEntry inst = new PluginEntry( (String) pd.get("pluginId",  
    			getDefaultPluginIdFromName(pd)));
    	
    	inst.versions = versionsJSONfromPluginDescriptor(pd);
    	inst.installedVersion = pd.getPluginVersion();
    	inst.candidateVersion = pd.getPluginVersion();
    	
    	inst.installedPath = pd.getFileSystem().getRoot().toString();
        inst.name = pd.getPluginName();
        inst.description = (String) pd.getDescription(de.grogra.util.Described.NAME);
        
        
        inst.helpUrl = "https://gitlab.com/grogra/groimp/";
        inst.vendor = pd.getPluginProvider();
        inst.canUninstall = true;
        return inst;
    }
    
    
    public static JSONObject versionsJSONfromPluginDescriptor(PluginDescriptor pd) {
    	JSONObject versions = new JSONObject();
    	JSONObject v = new JSONObject();
    	JSONObject libs = new JSONObject();
    	Item[] libraries = pd.findAll (new ItemCriterion (){
			@Override
			public boolean isFulfilled (Item item, Object info){
				return (item instanceof Library);}
			@Override
			public String getRootDirectory (){
				return null;}
		}, null, false);  
    	for (Object lib : libraries) {
    		for ( Map.Entry<String[], String[]> entry : ((Library)lib).getLibrary().entrySet() ) {
    			libs.put(entry.getKey()[0], 
    					// list of prefixes
    					// entry.getValue()
    					// path to jar (kinda :-( )
    					pd.getFileSystem().getRoot().toString()+"lib"+"entry.getKey()[0]"
    					);
    		}
    	}
    	
    	List<String> deps = new ArrayList<String>();
    	Item[] prerequises = pd.findAll (new ItemCriterion (){
			@Override
			public boolean isFulfilled (Item item, Object info){
				return (item instanceof PluginPrerequisite);}
			@Override
			public String getRootDirectory (){
				return null;}
		}, null, false);  
    	
    	for (Item pp : prerequises) {
    		deps.add(pp.getName());
    	}
    	
    	List<String> excluded = new ArrayList<String>();
    	Item[] exclusions = pd.findAll (new ItemCriterion (){
			@Override
			public boolean isFulfilled (Item item, Object info){
				return (item instanceof Exclude);}
			@Override
			public String getRootDirectory (){
				return null;}
		}, null, false);  
    	
    	for (Item pe : exclusions) {
    		excluded.add(pe.getName());
    	}
    	
    	
    	v.put("downloadUrl", pd.getFileSystem().getRoot().toString());
    	v.put("libs", libs);
    	v.put("depends", deps);
    	v.put("excludes", excluded);
    	
    	versions.put(pd.getPluginVersion(), v);
    	return versions;
    }
    
    
    public static String getDefaultPluginIdFromName(PluginDescriptor pd) {
    	return pd.getPluginProvider()+"."+pd.getName().toLowerCase();
    }
    

    @Override
    public String toString() {
        return id;
    }

    public void detectInstalled() {
        detectInstalledPlugin();

        if (isInstalled()) {
            candidateVersion = installedVersion;
        } else {
            candidateVersion = getMaxVersion();
        }
    }
    

    private void detectInstalledPlugin() {
    	Registry r = Main.getRegistry();

    	PluginDescriptor pd = (PluginDescriptor) Item.findFirst(r, "/plugins", new ItemCriterion ()
		{
			@Override
			public boolean isFulfilled (Item item, Object info)
			{
				return (item instanceof PluginDescriptor) ?
						((PluginDescriptor) item).getPluginName().equals(name)
						: false;
			}
			@Override
			public String getRootDirectory() {return null;}
		}, name, false);
    	
    	if (pd != null) {
       		installedPath = pd.getFileSystem().getRoot().toString();
        	installedVersion = pd.getPluginVersion();
    	}
    }


    public boolean isVersionFrozenToGroIMP() {
        return versions.has("");
    }

    public String getMaxVersion() {
        Set<String> versions = getVersions();
        if (versions.size() > 0) {
            String[] vers = versions.toArray(new String[0]);
            return vers[vers.length - 1];
        }
        return null;
    }

    public Set<String> getVersions() {
        Set<String> versions = new TreeSet<>(new VersionComparator());
        for (Object o : this.versions.keySet()) {
            if (o instanceof String) {
                String ver = (String) o;
                if (ver.isEmpty()) {
                    versions.add(getGroIMPVersion());
                } else {
                    versions.add(ver);
                }
            }
        }

        if (isInstalled()) {
            versions.add(installedVersion);
        }
        return versions;
    }

    public static String getGroIMPVersion() {
        String ver = de.grogra.pf.boot.Main.getVersion();
        String[] parts = ver.split(" ");
        if (parts.length > 1) {
            return parts[0];
        }

        return ver;
    }

//    public static String getVersionFromPath(String installedPath) {
//        Pattern p = Pattern.compile("-([\\.0-9a-zA-Z]+(-[\\w]+)?)");
//        Matcher m = p.matcher(installedPath);
//        if (m.find()) {
//            return m.group(1);
//        }
//        return VER_STOCK;
//    }

//    public static String getJARPath(String className) {
//        Class<?> cls;
//        try {
//            log.debug("Trying: " + className);
//            cls = Thread.currentThread().getContextClassLoader().loadClass(className);
//        } catch (Throwable e) {
//            if (e instanceof ClassNotFoundException) {
//                log.debug("Plugin not found by class: " + className);
//            } else {
//                log.warn("Unable to load class: " + className, e);
//            }
//            return null;
//        }
//
//        String file = cls.getProtectionDomain().getCodeSource().getLocation().getFile();
//        if (!file.toLowerCase().endsWith(".jar")) {
//            log.warn("Path is not JAR: " + file);
//        }
//
//        return file;
//    }

    public static String getLibInstallPath(String lib) {
        String[] cp = dependencies;
        String path = getLibPath(lib, cp);
        if (path != null) return path;
        return null;
    }

    public static String getLibPath(String lib, String[] paths) {
        for (String path : paths) {
            Pattern p = Pattern.compile("\\W" + lib + "-([0-9]+\\..+).jar");
            Matcher m = p.matcher(path);
            if (m.find()) {
                Main.getLogger().info("Found library " + lib + " at " + path);
                return path;
            }
        }
        return null;
    }

    public String getID() {
        return id;
    }

    public String getInstalledPath() {
        return installedPath;
    }

    public String getDestName() {
        return destName;
    }

    public String getTempName() {
        return tempName;
    }

    public boolean isInstalled() {
        return installedPath != null;
    }

    public void download(JSONSource jsonSource, GenericCallback<String> notify) throws IOException {

        String version = getCandidateVersion();

        String location = getDownloadUrl(version);

        JSONSource.DownloadResult dwn = jsonSource.getZip(id, location, notify);
        tempName = dwn.getTmpFile();
        if (!checkZip(version)) {
        	throw new DownloadException("Version mismatched between candidate and zip for :" + dwn.getName());
        }
        if (installedPath == null) {
            File f = new File(Main.getProperty(Main.USER_PLUGIN_PATH));
            destName = URLDecoder.decode(f.toString(), "UTF-8") + File.separator + dwn.getName();
        }
        else {
        	destName = installedPath;
        }
    }

    /**
     * @param version
     * @return
     */
    public String getDownloadUrl(String version) {
        String location;
        if (isVersionFrozenToGroIMP()) {
            String downloadUrl = versions.getJSONObject("").getString("downloadUrl");
            location = String.format(downloadUrl, getGroIMPVersion());
        } else {
            if (!versions.has(version)) {
                throw new IllegalArgumentException("Version " + version + " not found for plugin " + this);
            }
            location = versions.getJSONObject(version).getString("downloadUrl");
        }
        return location;
    }
    
    //TODO: md5 + check plugin names and install
    // for now only check that plugin loads & its version - but should check some md5
    public boolean checkZip(String version) throws IOException{
    	try (ZipFile zipFile = new ZipFile(tempName)) {
			Enumeration<? extends ZipEntry> entries = zipFile.entries();

			while(entries.hasMoreElements()){
			    ZipEntry entry = entries.nextElement();
			    if (entry.getName().equals("plugin.xml")) {
			    	InputStream in = zipFile.getInputStream(entry);
			    	PluginDescriptor pd = PluginDescriptor.read ("temp-"+name,
							new BufferedInputStream (in), null, null);
			    	
			    	in.close();
			    	if (pd.getPluginVersion().equals(version)) {
				    	return true;
			    	}
			    }            
			}
		}
    	return false;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public String getHelpLink() {
        return helpUrl;
    }

    public String getVendor() {
        return vendor;
    }

    public String getCandidateVersion() {
        return candidateVersion;
    }

    public boolean canUninstall() {
        return canUninstall;
    }

    public String getInstalledVersion() {
        return installedVersion;
    }
    
    public String getMainClass() {
    	return mainClass;
    }

    public void setCandidateVersion(String candidateVersion) {
        this.candidateVersion = candidateVersion;
    }

    public boolean isUpgradable() {
        if (!isInstalled()) {
            return false;
        }

        VersionComparator comparator = new VersionComparator();
        return comparator.compare(getInstalledVersion(), getMaxVersion()) < 0;
    }

    public Set<String> getDepends() {
        Set<String> depends = new HashSet<>();
        JSONObject version = versions.getJSONObject(getCandidateVersion());
        if (version.has("depends")) {
            JSONArray list = version.getJSONArray("depends");
            for (Object o : list) {
                if (o instanceof String) {
                    String dep = (String) o;
                    Matcher m = dependsParser.matcher(dep);
                    if (!m.find()) {
                        throw new IllegalArgumentException("Cannot parse depend str: " + dep);
                    }
                    depends.add(m.group(1));
                }
            }
        }
        return depends;
    }
    

    public Set<String> getExcluded() {
        Set<String> excluded = new HashSet<>();
        JSONObject version = versions.getJSONObject(getCandidateVersion());
        if (version.has("excludes")) {
            JSONArray list = version.getJSONArray("excludes");
            for (Object o : list) {
                if (o instanceof String) {
                    String excl = (String) o;
                    Matcher m = dependsParser.matcher(excl);
                    if (!m.find()) {
                        throw new IllegalArgumentException("Cannot parse exclude str: " + excl);
                    }
                    excluded.add(m.group(1));
                }
            }
        }
        return excluded;
    }
    
    

    public Map<String, String> getLibs(String verStr) {
        Map<String, String> depends = new HashMap<>();
        JSONObject version = versions.getJSONObject(isVersionFrozenToGroIMP() ? "" : verStr);
        if (version.has("libs")) {
            JSONObject list = version.getJSONObject("libs");
            for (Object o : list.keySet()) {
                if (o instanceof String) {
                    String dep = (String) o;
                    depends.put(dep, list.getString(dep));
                }
            }
        }
        return depends;
    }

    public Map<String, String> getRequiredLibs(String verStr) {
        Map<String, String> libs = getLibs(verStr);
        Map<String, String> requiredLibs = new HashMap<>();
        for (String libName : libs.keySet()) {
            if (libName.contains(">=")) {
                requiredLibs.put(libName, libs.get(libName));
            }
        }
        return requiredLibs;
    }

    public String getVersionChanges(String versionStr) {
        JSONObject version = versions.getJSONObject(versionStr);
        return version.has("changes") ?
                version.getString("changes") :
                null;
    }

    private class VersionComparator implements java.util.Comparator<String> {
        @Override
        public int compare(String a, String b) {
            String[] aParts = a.split("\\W+");
            String[] bParts = b.split("\\W+");

            for (int aN = 0; aN < aParts.length; aN++) {
                if (aN < bParts.length) {
                    int res = compare2(aParts[aN], bParts[aN]);
                    if (res != 0) return res;
                }
            }

            return a.compareTo(b);
        }

        private int compare2(String a, String b) {
            if (a.equals(b)) {
                return 0;
            }

            Object ai, bi;
            try {
                ai = Integer.parseInt(a);
            } catch (NumberFormatException e) {
                ai = a;
            }

            try {
                bi = Integer.parseInt(b);
            } catch (NumberFormatException e) {
                bi = b;
            }

            if (ai instanceof Integer && bi instanceof Integer) {
                return Integer.compare((Integer) ai, (Integer) bi);
            } else if (ai instanceof String && bi instanceof String) {
                return ((String) ai).compareTo((String) bi);
            } else if (ai instanceof String) {
                return 1;
            } else {
                return -1;
            }
        }
    }
    
    public String getSearchIndexString() {
        if (searchIndexString == null || searchIndexString.isEmpty()) {
            StringBuilder builder = new StringBuilder();
            builder.append(id);
            builder.append(name);
            builder.append(description);
            searchIndexString = builder.toString().toLowerCase();
        }

        return searchIndexString;
    }

}
