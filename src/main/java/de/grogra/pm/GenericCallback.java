package de.grogra.pm;

public interface GenericCallback<T> {
    void notify(T t);
}