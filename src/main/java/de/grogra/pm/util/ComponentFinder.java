package de.grogra.pm.util;


import java.awt.*;

import de.grogra.pf.boot.Main;

public class ComponentFinder<T extends Component> {

    private final Class<T> search;

    public ComponentFinder(Class<T> cls) {
        search = cls;
    }

    public T findComponentIn(Container container) {
        Main.getLogger().warning("Searching in " + container);
        for (Component a : container.getComponents()) {
            if (search.isAssignableFrom(a.getClass())) {
            	Main.getLogger().warning("Found " + a);
                return (T) a;
            }

            if (a instanceof Container) {
                T res = findComponentIn((Container) a);
                if (res != null) {
                    return res;
                }
            }
        }

        return null;
    }
}
