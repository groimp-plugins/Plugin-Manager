package de.grogra.pm.cache;

import org.apache.commons.io.FileUtils;

import de.grogra.pf.boot.Main;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;

public class PluginsRepo implements Serializable {
    private static final long serialVersionUID = 1L;

    private final String repoJSON;
    private final long expirationTime;
    private final long lastModified;

    public PluginsRepo(String repoJSON, long expirationTime, long lastModified) {
        this.repoJSON = repoJSON;
        this.expirationTime = expirationTime;
        this.lastModified = lastModified;
    }

    public boolean isActual() {
        return expirationTime > System.currentTimeMillis();
    }

    public boolean isActual(long lastModified) {
        return isActual() && lastModified <= this.lastModified;
    }

    public long getExpirationTime() {
        return expirationTime;
    }

    public String getRepoJSON() {
        return repoJSON;
    }

    public void saveToFile(File file) {
//        log.debug("Saving repo to file: " + file.getAbsolutePath());
        // Serialization
        try (FileOutputStream fout = new FileOutputStream(file);
             ObjectOutputStream out = new ObjectOutputStream(fout)) {
            FileUtils.touch(file);
            // Method for serialization of object
            out.writeObject(this);
        } catch (IOException ex) {
        	Main.getLogger().warning("Failed for serialize repo"+ ex);
        }
    }

    public static PluginsRepo fromFile(File file) {
        // Deserialization
        try (FileInputStream fis = new FileInputStream(file);
             ObjectInputStream in = new ObjectInputStream(fis);) {

            // Method for deserialization of object
            return (PluginsRepo) in.readObject();
        } catch (IOException | ClassNotFoundException ex) {
            Main.getLogger().warning("Failed for deserialize repo"+ ex);
            return null;
        }
    }
}
