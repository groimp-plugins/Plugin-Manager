package de.grogra.pm;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.URLDecoder;
import java.nio.file.AccessDeniedException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.TreeSet;
import java.util.logging.Logger;

import org.json.JSONArray;
import org.json.JSONObject;
import de.grogra.pf.boot.Main;
import de.grogra.pf.registry.PluginDescriptor;
import de.grogra.pf.registry.Registry;
import de.grogra.pm.exception.DownloadException;

public class PluginManager {
	
	private static PluginManager staticManager = new PluginManager();
    private final JSONSource jsonSource;
    protected Map<PluginEntry, Boolean> allPlugins = new HashMap<>();
    private final Properties props;    
	public final static String REPO_PLUGIN_LIST = "repo-plugin-list";
	private static Logger log;
	private String pluginsDirectory;

	public final static String DEFAULT_PLUGIN_LIST = 
			"https://gitlab.com/gaetan.heidsieck/groimp/-/raw/master/plugin_repo.list";
    
	public PluginManager() {

		Properties config = new Properties ();
		Properties p = new Properties (config);
		props = p;
		log = de.grogra.pf.boot.Main.getLogger();
		pluginsDirectory=Main.getProperty (de.grogra.pf.boot.Main.PLUGIN_ROOT_PATH);

        String defaultProp = p.getProperty(REPO_PLUGIN_LIST, DEFAULT_PLUGIN_LIST);
        String groimpProp = de.grogra.pf.boot.Main.getProperty(REPO_PLUGIN_LIST, defaultProp);
        File jsonFile = new File(groimpProp);
        if (jsonFile.isFile()) {
            jsonSource = new JSONSourceLocal(jsonFile);
        } else {
        	jsonSource = new JSONSourceHTTP(groimpProp);
        }
    }
	
	public boolean hasPlugins() {
        return allPlugins.size() > 0;
    }
	
	
	public synchronized void load() throws Throwable {
//		detectJARConflicts();

        if (hasPlugins()) {
            return;
        }
        
        // load already installed plugins

        JSONArray json = jsonSource.getRepo();
        
        allPlugins.putAll( getAlreadyInstalledPlugins() );

        if (!(json instanceof JSONArray)) {
            throw new RuntimeException("Result is not array");
        }

        for (Object elm : (JSONArray) json) {
            if (elm instanceof JSONObject) {
            	PluginEntry plugin = PluginEntry.fromJSON((JSONObject) elm);
                if (plugin.getName().isEmpty()) {
                    log.warning("Skip empty name: " + plugin);
                    continue;
                }

                plugin.detectInstalled();
                if ( !containsPlugin( plugin.getID())){
                	allPlugins.put(plugin, plugin.isInstalled());
                }
            	else {
            		getPluginByID(plugin.getID()).versions.put(plugin.getMaxVersion(), 
            				plugin.versions.get(plugin.getMaxVersion())) ;
            	}
            } else {
                log.warning("Invalid array element: " + elm);
            }
        }
	}
	
	
	public Boolean containsPlugin(String id) {
		for (PluginEntry plugin : allPlugins.keySet()) {
            if (plugin.getID().equals(id)) {
                return true;
            }
        }
		return false;
	}

    private void checkRW() throws UnsupportedEncodingException, AccessDeniedException {
        String pPath = pluginsDirectory;
        if (pPath != null) {
            File libext = new File(URLDecoder.decode(pPath, "UTF-8"));
            if (!isWritable(libext)) {
                String msg = "Have no write in GroIMP directory, not possible to use Plugins Manager: ";
                throw new AccessDeniedException(msg + libext);
            }
        }
    }

    private boolean isWritable(File path) {
        File sample = new File(path, "empty.txt");
        try {
            sample.createNewFile();
            sample.delete();
            return true;
        } catch (IOException e) {
            log.severe("Not writable repository");
            return false;
        }
    }
    

    public void startModifications(Set<PluginEntry> delPlugins, Set<PluginEntry> installPlugins, Set<Library.InstallationInfo> installLibs,
                                   Set<String> libDeletions, boolean doRestart, LinkedList<String> additionalGroIMPptions) throws IOException {
        ChangesMaker maker = new ChangesMaker(allPlugins);
        File moveFile = maker.getMovementsFile(delPlugins, installPlugins, installLibs, libDeletions);
        File installFile = maker.getInstallFile(installPlugins, installLibs);
        File restartFile;
        if (doRestart) {
            restartFile = maker.getRestartFile(additionalGroIMPptions);
        } else {
            restartFile = null;
        }
        final ProcessBuilder builder = maker.getProcessBuilder(moveFile, installFile, restartFile);
        log.info("JAR Modifications log will be saved into: " + builder.redirectOutput().file().getPath());
        builder.start();
    }

    public void applyChanges(GenericCallback<String> statusChanged, boolean doRestart, LinkedList<String> additionalGroIMPptions) {
        try {
            checkRW();
        } catch (Throwable e) {
            throw new RuntimeException("Cannot apply changes: " + e.getMessage(), e);
        }

        DependencyResolver resolver = new DependencyResolver(allPlugins);
        Set<PluginEntry> additions = resolver.getAdditions();
        Set<Library.InstallationInfo> libInstalls = new HashSet<>();

        // not managed yet (the libs needs to be included in the plugin zip)
        for (Map.Entry<String, String> entry : resolver.getLibAdditions().entrySet()) {
            try {
                JSONSource.DownloadResult dwn = jsonSource.getZip(entry.getKey(), entry.getValue(), statusChanged);
                libInstalls.add(new Library.InstallationInfo(entry.getKey(), dwn.getTmpFile(), dwn.getFilename()));
            } catch (Throwable e) {
                String msg = "Failed to download " + entry.getKey();
                log.warning(msg+ e);
                statusChanged.notify(msg);
                throw new DownloadException("Failed to download library " + entry.getKey(), e);
            }
        }

        for (PluginEntry plugin : additions) {
            try {
                plugin.download(jsonSource, statusChanged);
            } catch (IOException e) {
                String msg = "Failed to download " + plugin;
                log.warning(msg + e);
                statusChanged.notify(msg);
                throw new DownloadException("Failed to download plugin " + plugin, e);
            }
        }

        if (doRestart) {
            log.info("Restarting GroIMP...");
            statusChanged.notify("Restarting GroIMP...");
        }

        Set<String> libDeletions = new HashSet<>();
        for (String lib : resolver.getLibDeletions()) {
            libDeletions.add(PluginEntry.getLibInstallPath(lib));
        }

        modifierHook(resolver.getDeletions(), additions, libInstalls, libDeletions, doRestart, additionalGroIMPptions);
    }

    private void modifierHook(final Set<PluginEntry> deletions, final Set<PluginEntry> additions, final Set<Library.InstallationInfo> libInstalls,
                              final Set<String> libDeletions, final boolean doRestart, final LinkedList<String> additionalGroIMPptions) {
        if (deletions.isEmpty() && additions.isEmpty() && libInstalls.isEmpty() && libDeletions.isEmpty()) {
            log.info("Finishing without changes");
        } else {
            log.info("Plugins manager will apply some modifications");
            Runtime.getRuntime().addShutdownHook(new Thread() {
                @Override
                public void run() {
                    try {
                        log.info("Starting GroIMP Plugins modifications");
                        startModifications(deletions, additions, libInstalls, libDeletions, doRestart, additionalGroIMPptions);
                    } catch (Exception e) {

                    }
                }
            });
            // change spot
            if (doRestart) {
            	Main.exit();
            }
        }
    }


    public String getChangesAsText() {
        DependencyResolver resolver = new DependencyResolver(allPlugins);

        StringBuilder text = new StringBuilder();

        for (PluginEntry pl : resolver.getDeletions()) {
            text.append("Uninstall plugin: ").append(pl).append(" ").append(pl.getInstalledVersion()).append("\n");
        }

        for (String pl : resolver.getLibDeletions()) {
            text.append("Uninstall library: ").append(pl).append("\n");
        }

        for (String pl : resolver.getLibAdditions().keySet()) {
            text.append("Install library: ").append(pl).append("\n");
        }

        for (PluginEntry pl : resolver.getAdditions()) {
            text.append("Install plugin: ").append(pl).append(" ").append(pl.getCandidateVersion()).append("\n");
        }
        
        for (Map.Entry<PluginEntry, PluginEntry> entry : resolver.getPluginExclusions().entrySet()) {
            if (entry.getValue() != null) {
	        	text.append("Plugin : ").append(entry.getKey().getName())
	            .append(" cannot be installed because it excludes the plugin :")
	            .append(entry.getValue().getName()).append("\n");
            }
            else {
            	text.append("Plugin : ").append(entry.getKey().getName())
	            .append(" cannot be installed because it excludes an installed plugin").append("\n");
            }
        }

        return text.toString();
    }
    
    
    public HashMap<PluginEntry, Boolean> getAlreadyInstalledPlugins() {
    	HashMap<PluginEntry, Boolean> result = new HashMap<>();
    	
    	Registry r = Main.getRegistry();
		PluginDescriptor d = (PluginDescriptor)
			r.getPluginDirectory ().getBranchTail ();
		while (d != null)
		{
			if (d.getPluginState () != PluginDescriptor.DISABLED 
					&& !d.getPluginName ().equals("Core Classes"))
			{
				try
				{
					PluginEntry pe = PluginEntry.fromPluginDescriptor(d);
					result.put(pe, true);
				}
				catch (Exception e)
				{
					Main.logSevere (e);
				}
			}
			d = (PluginDescriptor) d.getPredecessor ();
		}
        return result;
    }

    public Set<PluginEntry> getInstalledPlugins() {
		Set<PluginEntry> result = new TreeSet<>(new PluginComparator());
	    for (PluginEntry plugin : allPlugins.keySet()) {
	        if (plugin.isInstalled()) {
	            result.add(plugin);
	        }
	    }
	    return result;
    }

    public static Set<PluginEntry> getInstalledPlugins(Map<PluginEntry, Boolean> allPlugins) {
        Set<PluginEntry> result = new HashSet<>();
        for (PluginEntry plugin : allPlugins.keySet()) {
            if (plugin.isInstalled()) {
                result.add(plugin);
            }
        }
        return result;
    }

    public Set<PluginEntry> getAvailablePlugins() {
        Set<PluginEntry> result = new TreeSet<>(new PluginComparator());
        for (PluginEntry plugin : allPlugins.keySet()) {
            if (!plugin.isInstalled()) {
                result.add(plugin);
            }
        }
        return result;
    }

    public Set<PluginEntry> getUpgradablePlugins() {
        Set<PluginEntry> result = new TreeSet<>(new PluginComparator());
        for (PluginEntry plugin : allPlugins.keySet()) {
            if (plugin.isUpgradable()) {
                result.add(plugin);
            }
        }
        return result;
    }

    public void togglePlugins(Set<PluginEntry> pluginsToInstall, boolean isInstall) {
        for (PluginEntry plugin : pluginsToInstall) {
            toggleInstalled(plugin, isInstall);
        }
    }

    public void toggleInstalled(PluginEntry plugin, boolean cbState) {
        if (!cbState && !plugin.canUninstall()) {
            log.warning("Cannot uninstall plugin: " + plugin);
            cbState = true;
        }
        allPlugins.put(plugin, cbState);
    }

    public boolean hasAnyUpdates() {
        for (PluginEntry p : allPlugins.keySet()) {
            if (p.isUpgradable()) {
                return true;
            }
        }
        return false;
    }

    public PluginEntry getPluginByID(String key) {
        for (PluginEntry p : allPlugins.keySet()) {
            if (p.getID().equals(key)) {
                return p;
            }
        }
        throw new IllegalArgumentException("Plugin not found in repo: " + key);
    }

    private class PluginComparator implements java.util.Comparator<PluginEntry> {
        @Override
        public int compare(PluginEntry o1, PluginEntry o2) {
            return o1.getName().compareTo(o2.getName());
        }
    }

    public void setTimeout(int timeout) {
        jsonSource.setTimeout(timeout);
    }

    /**
     * @return Static instance of manager, used to spare resources on repo loading
     */
    public static PluginManager getStaticManager() {
        try {
            staticManager.load();
        } catch (Throwable e) {
            throw new RuntimeException("Failed to get plugin repositories", e);
        }
        return staticManager;
    }

    /**
     * @param id ID of the plugin to check
     * @return Version name for the plugin if it is installed, null otherwise
     */
    public static String getPluginStatus(String id) {
        PluginManager manager = getStaticManager();

        for (PluginEntry plugin : manager.allPlugins.keySet()) {
            if (plugin.id.equals(id)) {
                return plugin.getInstalledVersion();
            }
        }
        return null;
    }

    /**
     * @return Status for all plugins
     */
    public static String getAllPluginsStatus() {
        PluginManager manager = getStaticManager();
        return manager.getAllPluginsStatusString();
    }

    private String getAllPluginsStatusString() {
        ArrayList<String> res = new ArrayList<>();
        for (PluginEntry plugin : getInstalledPlugins()) {
            res.add(plugin.getID() + "=" + plugin.getInstalledVersion());
        }
        return Arrays.toString(res.toArray());
    }
    
    public Map<PluginEntry, Boolean> getAllPlugins() {
    	return allPlugins;
    }

    /**
     * @return Available plugins
     */
    public static String getAvailablePluginsAsString() {
        PluginManager manager = getStaticManager();
        return manager.getAvailablePluginsString();
    }

    private String getAvailablePluginsString() {
        ArrayList<String> res = new ArrayList<>();
        for (PluginEntry plugin : getAvailablePlugins()) {
            List<String> versions = new ArrayList<>(plugin.getVersions());
            Collections.reverse(versions);
            res.add(plugin.getID() + "=" + Arrays.toString(versions.toArray()));
        }
        return Arrays.toString(res.toArray());
    }

    /**
     * @return Upgradable plugins
     */
    public static String getUpgradablePluginsAsString() {
        PluginManager manager = getStaticManager();
        return manager.getUpgradablePluginsString();
    }

    private String getUpgradablePluginsString() {
        ArrayList<String> res = new ArrayList<>();
        for (PluginEntry plugin : getUpgradablePlugins()) {
            res.add(plugin.getID() + "=" + plugin.getMaxVersion());
        }
        return (res.size() != 0) ?
                Arrays.toString(res.toArray()) :
                "There is nothing to update.";
    }


//    public static void detectJARConflicts() {
//        String[] paths = System.getProperty("java.class.path").split(File.pathSeparator);
//        final Map<String, String> jarNames = new HashMap<>();
//
//        for (String path : paths) {
//            String name = path;
//            int start = path.lastIndexOf(File.separator);
//            if (start > 0) {
//                name = name.substring(start + 1);
//            }
//            if (path.endsWith(".jar")) {
//                name = name.substring(0, name.length() - 4);
//            }
//            name = removeJARVersion(name);
//            if (jarNames.containsKey(name)) {
//                log.warning("Found JAR conflict: " + path + " and " + jarNames.get(name));
//            }
//            jarNames.put(name, path);
//        }
//    }

//    protected static String removeJARVersion(String path) {
//        StringBuilder result = new StringBuilder();
//        String data[] = path.split("-");
//        for (int i = 0; i < data.length; i++) {
//            String ch = data[i];
//            if (!ch.isEmpty() && (!Character.isDigit(ch.charAt(0)) || (i < data.length - 1))) {
//                result.append(ch);
//            }
//        }
//        return result.toString();
//    }

    public void logPluginComponents() {
        StringBuilder report = new StringBuilder("Plugin Components:\n");
        for (PluginEntry plugin : getInstalledPlugins()) {
            String[] searchPaths = {plugin.installedPath};
        }
    }
}
