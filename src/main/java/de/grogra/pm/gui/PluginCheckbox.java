package de.grogra.pm.gui;

import javax.swing.JCheckBox;

import de.grogra.pm.PluginEntry;

class PluginCheckbox extends JCheckBox {

    /**
     *
     */
    private static final long serialVersionUID = 3604852617806921883L;
    private PluginEntry plugin;

    public PluginCheckbox(String name) {
        super(name);
    }

    public void setPlugin(PluginEntry plugin) {
        this.plugin = plugin;
        if (!plugin.canUninstall()) {
            super.setEnabled(false);
        }
    }

    public PluginEntry getPlugin() {
        return plugin;
    }

    @Override
    public void setEnabled(boolean b) {
        if (!plugin.canUninstall()) {
            super.setEnabled(false);
        } else {
            super.setEnabled(b);
        }
    }
}
