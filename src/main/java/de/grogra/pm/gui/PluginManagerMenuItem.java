package de.grogra.pm.gui;

import de.grogra.pf.boot.Main;
import de.grogra.pf.registry.Item;
import de.grogra.pf.ui.Context;
import de.grogra.pf.ui.Panel;
import de.grogra.pf.ui.swing.PanelSupport;
import de.grogra.pf.ui.swing.SwingPanel;
import de.grogra.pf.ui.swing.WindowSupport;
import de.grogra.pf.ui.UIToolkit;
import de.grogra.pm.PluginManager;
import de.grogra.util.Map;

import javax.swing.*;

import java.awt.Container;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class PluginManagerMenuItem extends JMenuItem implements ActionListener {
    private static final long serialVersionUID = -8708638472918746046L;
    private static PluginManagerDialog dialog;
    private static final PluginManager mgr = new PluginManager();

    public PluginManagerMenuItem() {
        super("Plugins Manager");
        addActionListener(this);

        final JButton toolbarButton = getToolbarButton();

        new Thread("repo-downloader-thread") {
            @Override
            public void run() {
                try {
                    mgr.load();
                } catch (Throwable e) {
                    Main.getLogger().warning("Failed to load plugin updates info"+ e);
                }

                if (mgr.hasAnyUpdates()) {
                    setText("Plugins Manager (has upgrades)");
//                    Main.getLogger().warning("Plugins Manager has upgrades: " + Arrays.toString(mgr.getUpgradablePlugins().toArray()));
                }

                boolean hasAnyUpdates = mgr.hasAnyUpdates();
                toolbarButton.setToolTipText(hasAnyUpdates ?
                        "Plugins Manager (has upgrades)" :
                        "Plugins Manager"
                );
            }
        }.start();
    }


    private JButton getToolbarButton() {
        boolean hasAnyUpdates = mgr.hasAnyUpdates();
        JButton button = new JButton();
        button.setToolTipText(hasAnyUpdates ?
                "Plugins Manager (has upgrades)" :
                "Plugins Manager"
        );
        button.addActionListener(this);
        return button;
    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (dialog == null) {
            dialog = new PluginManagerDialog(mgr);
        }

    	dialog.show(true, null);
    }
    
    
    public static void startPM(Item item, Object information, Context context) {
    	if (dialog == null) {
			dialog = new PluginManagerDialog(mgr);
        }

        // dialog.pack();
    	UIToolkit ui = UIToolkit.get(context);
		Panel p = ui.createPanel(context, null, Map.EMPTY_MAP);
		
		Container c = ((SwingPanel) p.getComponent()).getContentPane();
		((PanelSupport)p).initialize((WindowSupport) context.getWindow(), Map.EMPTY_MAP);
		
//    	((SwingPanelSupport)  dialog).initialize((SwingWindowSupport) context.getWindow(), Map.EMPTY_MAP);
    	dialog.show(true, null);
	}
}
