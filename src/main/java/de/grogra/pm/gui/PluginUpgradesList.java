package de.grogra.pm.gui;


import javax.swing.event.ChangeListener;

import de.grogra.pm.GenericCallback;
import de.grogra.pm.PluginEntry;

public class PluginUpgradesList extends PluginsList {
    /**
     *
     */
    private static final long serialVersionUID = 525391154129274758L;

    public PluginUpgradesList(GenericCallback<Object> dialogRefresh) {
        super(dialogRefresh);
    }

    @Override
    protected PluginCheckbox getCheckboxItem(PluginEntry plugin, ChangeListener changeNotifier) {
        PluginCheckbox checkboxItem = super.getCheckboxItem(plugin, changeNotifier);
        plugin.setCandidateVersion(plugin.getMaxVersion());
        checkboxItem.setSelected(true);
        return checkboxItem;
    }

    @Override
    protected void setUpVersionsList(PluginCheckbox cb) {
        super.setUpVersionsList(cb);
        version.setEnabled(false);
    }

    @Override
    protected String getCbVersion(PluginCheckbox cb) {
        if (cb.isSelected()) {
            return cb.getPlugin().getMaxVersion();
        } else {
            return cb.getPlugin().getInstalledVersion();
        }
    }
}
