package de.grogra.pm;

import java.io.File;
import java.io.IOException;
import org.apache.commons.io.FileUtils;
import org.json.JSONArray;

public class JSONSourceLocal extends JSONSource {
    private final File base;
    private final File jsonFile;

    public JSONSourceLocal(File jsonFile) {
        this.jsonFile = jsonFile;
        this.base = jsonFile.getParentFile();
    }

    @Override
    public JSONArray getRepo() throws IOException {
        return new JSONArray(FileUtils.readFileToString(jsonFile));
    }

    @Override
    public void setTimeout(int timeout) {
    }

	@Override
	public DownloadResult getZip(String id, String location, GenericCallback<String> statusChanged) throws IOException {
        File orig = new File(base.getAbsolutePath() + File.separator + location);
        File tmp = File.createTempFile("gipm-", ".zip");
        FileUtils.copyFile(orig, tmp);
        return new DownloadResult(tmp.getAbsolutePath(), orig.getName());
	}
}
