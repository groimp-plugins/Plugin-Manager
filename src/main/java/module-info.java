
module plugin.manager {
		
	exports de.grogra.pm;
	
	requires java.sql;
	requires java.desktop;
	requires java.management;	
	requires org.apache.commons.codec;
	requires org.apache.commons.io;
	requires org.json;
	requires org.apache.httpcomponents.httpcore;
	requires org.apache.httpcomponents.httpclient;
	requires org.apache.commons.text;
	requires org.apache.commons.lang3;

	requires platform.core;
	requires utilities;
	requires platform;
	requires platform.swing;
}